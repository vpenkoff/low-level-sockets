#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define PORT 1234
#define BUFSIZE 4096

  void
str_send(int *sockfd)
{
  char sendline[BUFSIZE];
  int bc;

  while(fgets(sendline, BUFSIZE, stdin) != NULL){
    bc = send(*sockfd, sendline, strlen(sendline), 0);
    if(bc < 0)
      perror("error with send");
    if(bc != strlen(sendline))
      perror("send failed");
    if(*sendline == '\n')
      break;

  }
  memset(sendline, 0, sizeof(sendline));
  close(*sockfd);
}


  int
main(int argc, char *argv[])
{
  struct sockaddr_in server;
  struct hostent *host_info;
  int sock;

  if(argc<2)
    perror("Usage: ./client server_addr");
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock<0)
    perror("Socket creation failed");

  memset(&server, 0, sizeof(server));

  if((inet_pton(AF_INET, argv[1], &server)) < 1){
    host_info = gethostbyname(argv[1]);
    if(NULL==host_info)
      perror("Server unknown");
    memcpy((char*)&server.sin_addr, host_info->h_addr,
        host_info->h_length);
  }

  server.sin_family = AF_INET;
  server.sin_port = htons(PORT);

  if(connect (sock, (struct sockaddr*)&server, sizeof(server)) < 0)
    perror("connect failed");


  str_send(&sock);
  close(sock);

  return EXIT_SUCCESS;
}

