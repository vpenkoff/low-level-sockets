#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define PORT 1234
#define BUFSIZE 4096

struct server_params
{
  int sockfd;
  int fd;
  int bc;
  char buf[BUFSIZE];
  unsigned int len;
  struct sockaddr_in client;
};

void
echo(int* client_socket){
  struct server_params p;


  while((p.bc = recv(*client_socket, &p.buf, BUFSIZE, 0) > 0)){

    printf("MSG: %s", p.buf);
    memset(&p.buf, 0, sizeof(p.buf));

    if(p.bc == -1 || p.bc == 0){
      perror("recv failed");
      break;
    }
    else
      continue;
  }
}

  void
*echo_thread(void* server_params)
{
  struct server_params* p = (struct server_params*)server_params;
  echo(&p->fd);
  close(p->fd);
  return NULL;

}

  int
main(int argc, char* argv[])
{
  struct server_params cli;
  struct sockaddr_in server;
  pthread_t thread_id;

  cli.sockfd=socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(cli.sockfd < 0)
    perror("Socket creation failed");

  memset(&server, 0, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(PORT);
  printf("Server-socket() is OK...\n");

  if(bind(cli.sockfd, (struct sockaddr*)&server, sizeof(server)) < 0)
    perror("bind failed");
  printf("Server-bind() is OK...\n");

  if(listen(cli.sockfd, 10) == -1)
    perror("listen failed");
  printf("Server-listen() is OK...\n");

  for(;;){
    if((cli.fd = accept(cli.sockfd, (struct sockaddr*)&cli.client,
            &cli.len)) < 0)
      perror("accept failed");
    printf("%s: New connection from %s on socket %d\n",
        argv[0], inet_ntoa(cli.client.sin_addr),
        cli.fd);

    if((pthread_create(&thread_id, NULL, &echo_thread, &cli)) != 0)
      perror("Pthread_create() failed");
    if((pthread_detach(thread_id)) != 0)
      perror("Pthread_detach() failed");
  }
  close(cli.sockfd);
  return EXIT_SUCCESS;
}
