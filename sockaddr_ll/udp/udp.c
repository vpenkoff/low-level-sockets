#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>           
#include <string.h>           

#include <netdb.h>            
#include <sys/types.h>        
#include <sys/socket.h>       
#include <netinet/in.h>       
#include <netinet/ip.h>       
#include <netinet/udp.h>     
#include <arpa/inet.h>       
#include <sys/ioctl.h>     
#include <bits/ioctls.h>   
#include <net/if.h>          
#include <linux/if_ether.h>   
#include <linux/if_packet.h>
#include <net/ethernet.h>

#include <errno.h>         

#define IP4_HLEN 20
#define UDP_HLEN 8

unsigned short int checksum(unsigned short int *, int); 
unsigned short int udp4_checksum(struct ip, struct udphdr, 
    unsigned char *, int);

int
main(int argc, char *argv[])
{
	unsigned char *src_mac, *dst_mac,  *ether_frame, *data;
	char *iface, *target, *src_ip, *dst_ip;
	int filedes, i, datalen, frame_length, bytes;
	struct ifreq ifr;
	struct sockaddr_ll device;
	struct ip iph;
	struct udphdr udph;
	
	src_mac = (unsigned char *)malloc(6 * sizeof(unsigned char));
	memset(src_mac, 0, sizeof(src_mac));
	dst_mac = (unsigned char *)malloc(6 * sizeof(unsigned char));
	memset(dst_mac, 0, sizeof(dst_mac));

	data = (unsigned char*)malloc((IP_MAXPACKET - IP4_HLEN - UDP_HLEN) * sizeof(unsigned char));
	memset(data, 0, (IP_MAXPACKET - IP4_HLEN - UDP_HLEN));
	
	ether_frame = (unsigned char*)malloc(IP_MAXPACKET * sizeof(unsigned char));
	memset(ether_frame, 0, IP_MAXPACKET * sizeof(unsigned char));

	iface = (char *)malloc(40 * sizeof(char));
	memset(iface , 0, 40 * sizeof(char));

	target = (char *)malloc(40 * sizeof(char));
	memset(target, 0, 40 * sizeof(char));

	src_ip = (char *)malloc(16 * sizeof(char));
	memset(src_ip, 0, 16 * sizeof(char));

	dst_ip = (char *)malloc(16 * sizeof(char));
	memset(dst_ip, 0, 16 * sizeof(char));

	strcpy(iface, "eth0");
	
	if ((filedes = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("socket creation failed");
		return(EXIT_FAILURE);
	}
	
	
	memset(&ifr, 0, sizeof(ifr));


	memcpy(ifr.ifr_name, iface, sizeof(iface));
	if (ioctl(filedes, SIOCGIFHWADDR, &ifr) < 0) {
		perror("ioctl failed to get MAC");
		return(EXIT_FAILURE);
	}
	close(filedes);
	
	memcpy(src_mac, ifr.ifr_hwaddr.sa_data, 6);
	
	printf("MAC ADDR for iface %s\t", iface);
	for (i = 0; i < 6; i++){
		printf("%02x:", src_mac[i]);
	}
	
	if((device.sll_ifindex = if_nametoindex(iface)) == 0){
		perror("if_nametoindex() failede");		
		return(EXIT_FAILURE);
	}
	printf("\tIndex for iface %s is %d\n", iface, device.sll_ifindex);
	
	dst_mac[0] = 0x00;
	dst_mac[1] = 0x25;
	dst_mac[2] = 0x64;
	dst_mac[3] = 0x63;
	dst_mac[4] = 0x59;
	dst_mac[5] = 0x14;
	
	strcpy(src_ip, "193.178.153.136");
	strcpy(dst_ip, "193.178.153.103");
	
	device.sll_family = AF_PACKET;
	memcpy(device.sll_addr, src_mac, 6);
	device.sll_halen = htons(6);

	datalen = 4;
	data[0] = 't';
	data[1] = 'e';
	data[2] = 's';
	data[3] = 't';
	
	
	iph.ip_hl = IP4_HLEN / (sizeof(unsigned long int));
	iph.ip_v = 4;
	iph.ip_tos = 0;
	iph.ip_len = htons(IP4_HLEN + UDP_HLEN + datalen);
	iph.ip_id = htons(0);
	iph.ip_off = 0;
	iph.ip_ttl = 255;
	iph.ip_p = IPPROTO_UDP;
	inet_pton(AF_INET, src_ip, &(iph.ip_src));
	inet_pton(AF_INET, dst_ip, &(iph.ip_dst));
	iph.ip_sum = 0;
	iph.ip_sum = checksum((unsigned short int*)&iph, IP4_HLEN);
	
	udph.source = htons(8887);
	udph.dest = htons(8887);
	udph.len = htons(UDP_HLEN + datalen);
	udph.check = udp4_checksum(iph, udph, data, datalen);
	
	frame_length = 6 + 6 + 2 + IP4_HLEN
	    + UDP_HLEN + datalen;

	memcpy(ether_frame, dst_mac, 6);
	memcpy(ether_frame + 6, src_mac, 6);
	ether_frame[12] = (ETH_P_IP/256);
	ether_frame[13] = (ETH_P_IP%256);
	
	memcpy(ether_frame + 14, &iph, IP4_HLEN);
	memcpy(ether_frame + 14 + IP4_HLEN, &udph, UDP_HLEN);
	memcpy(ether_frame + 14 + IP4_HLEN + UDP_HLEN, data, datalen);
	
	if ((filedes = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("socket creation failed");
		return EXIT_FAILURE;
	}
	if ((bytes = sendto(filedes, ether_frame, frame_length, 0,
	    (struct sockaddr*)&device, sizeof(device))) <= 0){
		perror("sendto failed");
		return EXIT_FAILURE;
	}

	close(filedes);
	
	free(src_mac);
	free(dst_mac);
	free(data);
	free(ether_frame);
	free(iface);
	free(target);
	free(src_ip);
	free(dst_ip);
	
	return 0;
}

unsigned short int
checksum(unsigned short int *addr, int len)
{
	int nleft = len;
	int sum = 0;
	unsigned short int *w = addr;
	unsigned short int answer = 0;
	
	while (nleft > 1){
		sum += *w++;
		nleft -= sizeof(unsigned short int);
	}
	
	if (nleft == 1){
		*(unsigned char *)(&answer) = *(unsigned char *)w;
		sum += answer;
	}
	
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	answer = ~sum;
	return answer;
}

unsigned short int
udp4_checksum(struct ip iph, struct udphdr udph, unsigned char *payload,
    int payloadlen)
{
	char buff[IP_MAXPACKET];
	char *ptr;
	int chksumlen = 0;
	int i;
	
	ptr = &buff[0];
	
	memcpy(ptr, &iph.ip_src.s_addr, sizeof(iph.ip_src.s_addr));
	ptr += sizeof(iph.ip_src.s_addr);
	chksumlen += sizeof(iph.ip_src.s_addr);
	
	memcpy(ptr, &iph.ip_dst.s_addr, sizeof(iph.ip_dst.s_addr));
	ptr += sizeof(iph.ip_dst.s_addr);
	chksumlen += sizeof(iph.ip_dst.s_addr);
	
	*ptr = 0;
	ptr++;
	chksumlen += 1;
	
	memcpy(ptr, &iph.ip_p, sizeof(iph.ip_p));
	ptr += sizeof(iph.ip_p);
	chksumlen += sizeof(iph.ip_p);

	memcpy(ptr, &udph.len, sizeof(udph.len));
	ptr += sizeof(udph.len);
	chksumlen += sizeof(udph.len);
	
	memcpy(ptr, &udph.source, sizeof(udph.source));
	ptr += sizeof(udph.source);
	chksumlen += sizeof(udph.source);

	memcpy(ptr, &udph.dest, sizeof(udph.dest));
	ptr += sizeof(udph.dest);
	chksumlen += sizeof(udph.dest);

	memcpy(ptr, &udph.len, sizeof(udph.len));
	ptr += sizeof(udph.len);
	chksumlen += sizeof(udph.len);

	*ptr = 0;
	ptr++;
	*ptr = 0;
	ptr++;
	chksumlen += 2;
	
	memcpy(ptr, payload, payloadlen);
	ptr += payloadlen;
	chksumlen += payloadlen;
	
	for (i = 0; i < payloadlen % 2; i++, ptr++){
		*ptr = 0;
		ptr++;
		chksumlen++;
	}
	return checksum((unsigned short int *)buff, chksumlen);
}

		
