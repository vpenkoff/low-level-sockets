#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <bits/ioctls.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>

#define IP4_HLEN 20
#define TCP_HLEN 20
#define IP_MAXPACKET 65535

 

struct ip
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    	unsigned int ip_hl:4;               /* header length */
    	unsigned int ip_v:4;                /* version */
#endif
#if __BYTE_ORDER == __BIG_ENDIAN
    	unsigned int ip_v:4;                /* version */
    	unsigned int ip_hl:4;               /* header length */
#endif
    	u_int8_t ip_tos;                    /* type of service */
    	u_short ip_len;                     /* total length */
    	u_short ip_id;                      /* identification */
    	u_short ip_off;                     /* fragment offset field */
#define IP_RF 0x8000                    /* reserved fragment flag */
#define IP_DF 0x4000                    /* dont fragment flag */
#define IP_MF 0x2000                    /* more fragments flag */
#define IP_OFFMASK 0x1fff               /* mask for fragmenting bits */
    	u_int8_t ip_ttl;                    /* time to live */
    	u_int8_t ip_p;                      /* protocol */
    	u_short ip_sum;                     /* checksum */
	struct in_addr ip_src, ip_dst; 
}__attribute__((packed));


struct tcphdr
{
    	u_int16_t source;
    	u_int16_t dest;
    	u_int32_t seq;
    	u_int32_t ack_seq;
#  if __BYTE_ORDER == __LITTLE_ENDIAN
    	u_int16_t res1:4;
    	u_int16_t doff:4;
    	u_int16_t fin:1;
    	u_int16_t syn:1;
    	u_int16_t rst:1;
    	u_int16_t psh:1;
    	u_int16_t ack:1;
    	u_int16_t urg:1;
    	u_int16_t res2:2;
#  elif __BYTE_ORDER == __BIG_ENDIAN
    	u_int16_t doff:4;
    	u_int16_t res1:4;
    	u_int16_t res2:2;
	u_int16_t urg:1;
    	u_int16_t ack:1;
    	u_int16_t psh:1;
   	u_int16_t rst:1;
    	u_int16_t syn:1;
    	u_int16_t fin:1;
#  else
#   error "Adjust your <bits/endian.h> defines"
#  endif
    	u_int16_t window;
    	u_int16_t check;
    	u_int16_t urg_ptr;
}__attribute__((packed));

struct pseudo_header{
	u_int32_t 	source_address;
	u_int32_t 	dest_address;
	u_int8_t 	placeholder;
	u_int8_t 	protocol;
	u_int16_t 	tcp_length;
};

unsigned short csum(unsigned short *, int);

int 
main(int argc, char *argv[])
{
	unsigned char *src_mac, *dst_mac, *ether_frame, *buff;
	char *iface, *src_ip, *dst_ip;
	int filedes, i, frame_len, bytes;
	struct ifreq ifr;
	struct sockaddr_ll device;
	struct ip *iph;
	struct tcphdr *tcph;
	struct pseudo_header psh;

	buff = (unsigned char *)malloc(sizeof(struct ip) + sizeof(struct tcphdr)
	    + IP4_HLEN + TCP_HLEN);
	
	iph = (struct ip*)buff;
	tcph = (struct tcphdr *)(buff + IP4_HLEN);

	src_mac = (unsigned char *)malloc(6 * sizeof(unsigned char));
	memset(src_mac, 0, sizeof(src_mac));
	dst_mac = (unsigned char *)malloc(6 * sizeof(unsigned  char));
	memset(dst_mac, 0, sizeof(dst_mac));
	ether_frame = (unsigned char*)malloc(IP_MAXPACKET *
	    sizeof(unsigned char));
	memset(ether_frame, 0, sizeof(ether_frame));

	iface = (char *)malloc(40 * sizeof(char));
	memset(iface, 0, sizeof(iface));
	
	src_ip = (char *)malloc(16 * sizeof(char));
	memset(src_ip, 0, sizeof(src_ip));
	
	dst_ip = (char *)malloc(16 * sizeof(char));
	memset(dst_ip, 0, sizeof(dst_ip));

	strcpy(iface, "eth0");
	
	if ((filedes = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("socket creation failed");
		return EXIT_FAILURE;
	}
	
	memset(&ifr, 0, sizeof(ifr));
	memcpy(&ifr.ifr_name, iface, sizeof(iface));
	if (ioctl(filedes, SIOCGIFHWADDR, &ifr) < 0){
		perror("ioctl failed to get mac");
		return EXIT_FAILURE;
	}
	close(filedes);
	
	memcpy(src_mac, ifr.ifr_hwaddr.sa_data, 6);
	
	printf("MAC ADDR FOR Iface %s\t", iface);
	for ( i = 0; i < 6; i++){
		printf("%02x ", src_mac[i]);
	}

	if((device.sll_ifindex = if_nametoindex(iface)) == 0){
		perror("if_nametoindex failed");
		return EXIT_FAILURE;
	}

	printf("\n index for iface %s is %d\n", iface, device.sll_ifindex);

	dst_mac[0] = 0x00;
	dst_mac[1] = 0x50;
	dst_mac[2] = 0x8b;
	dst_mac[3] = 0xe7;
	dst_mac[4] = 0xa6;
	dst_mac[5] = 0xf4;

	strcpy(src_ip, "193.178.153.136");
	strcpy(dst_ip, "193.178.153.253");

	device.sll_family = AF_PACKET;
	memcpy(device.sll_addr, src_mac, 6);
	device.sll_halen = htons(6);

	iph->ip_hl = IP4_HLEN / (sizeof(unsigned long int));
	iph->ip_v = 4;
	iph->ip_tos = 0;
	iph->ip_len = htons(IP4_HLEN + TCP_HLEN);
	iph->ip_id = htons(666);
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = IPPROTO_TCP;
	inet_pton(AF_INET, src_ip, &(iph->ip_src));
	inet_pton(AF_INET, dst_ip, &(iph->ip_dst));
	iph->ip_sum = 0;
	iph->ip_sum = csum((unsigned short int *)iph, IP4_HLEN);

	unsigned int p = 1;
	tcph->source = htons(8887);
	tcph->dest = htons(8887);
	//tcph->syn = p;
	tcph->doff = 5;
	tcph->ack = p;
	tcph->fin = p;
	tcph->check = 0;

	psh.source_address = inet_addr(src_ip);
	psh.dest_address = inet_addr(dst_ip);
	psh.placeholder = 0;
	psh.protocol = IPPROTO_TCP;
	psh.tcp_length = htons(sizeof(struct tcphdr));
	int psize = sizeof(struct pseudo_header) + sizeof(struct tcphdr);
	unsigned char *pseudogram = malloc(psize);
	
	memcpy(pseudogram , (char*) &psh , sizeof (struct pseudo_header));
	memcpy(pseudogram + sizeof(struct pseudo_header),tcph, 
	    sizeof(struct tcphdr));
	
	tcph->check = csum( (unsigned short*) pseudogram , psize);

	frame_len = ETH_HLEN + IP4_HLEN + TCP_HLEN;

	memcpy(ether_frame, dst_mac, 6);
	memcpy(ether_frame + 6, src_mac, 6);
	ether_frame[12] = (ETH_P_IP/256);
	ether_frame[13] = (ETH_P_IP%256);

	memcpy(ether_frame + 14, iph, IP4_HLEN);
	memcpy(ether_frame + 14 + IP4_HLEN, tcph, TCP_HLEN);
	memcpy(ether_frame + 14 + IP4_HLEN + TCP_HLEN, buff, sizeof(buff));

	if ((filedes = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("socket creation failed");
		return EXIT_FAILURE;
	}

	if ((bytes = sendto(filedes, ether_frame, frame_len, 0, 
	    (struct sockaddr*)&device, sizeof(device))) < 0){
		perror("sendto failed");	
		return EXIT_FAILURE;
	}
	
	close(filedes);
	free(src_mac);
	free(dst_mac);
	free(ether_frame);			 
	free(iface);
	free(src_ip);
	free(dst_ip);
	
	return EXIT_SUCCESS;
}

unsigned short csum(unsigned short *ptr,int nbytes) 
{
	register long sum;
	unsigned short oddbyte;
	register short answer;

	sum=0;
	while(nbytes>1) {
		sum+=*ptr++;
		nbytes-=2;
	}
	if(nbytes==1) {
		oddbyte=0;
		*((u_char*)&oddbyte)=*(u_char*)ptr;
		sum+=oddbyte;
	}

	sum = (sum>>16)+(sum & 0xffff);
	sum = sum + (sum>>16);
	answer=(short)~sum;
	
	return answer;
}
