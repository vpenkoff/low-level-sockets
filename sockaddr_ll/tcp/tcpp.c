#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <bits/ioctls.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>

#define IP4_HLEN 20
#define TCP_HLEN 20

unsigned short int checksum(unsigned short int *, int);
unsigned short int tcp_checksum(struct ip, struct tcphdr);

int
main(int argc, char *argv[])
{
	unsigned char *src_mac, *dst_mac, *ether_frame;
	char *iface, *src_ip, *dst_ip;
	int filedes, i, frame_len, bytes;
	struct ifreq ifr;
	struct sockaddr_ll device;
	struct ip iph;
	struct tcphdr tcph;
	
	src_mac = (unsigned char*)malloc(6 * sizeof(unsigned char));
	memset(src_mac, 0, sizeof(src_mac));
	dst_mac = (unsigned char*)malloc(6 * sizeof(unsigned char));
	memset(dst_mac, 0, sizeof(dst_mac));

	ether_frame = (unsigned char*)malloc(IP_MAXPACKET * sizeof(unsigned char));
	memset(ether_frame, 0, sizeof(ether_frame));

	iface = (char *)malloc(40 * sizeof(char));
	memset(iface, 0, sizeof(iface));

	src_ip = (char *)malloc(16 * sizeof(char));
	memset(src_ip, 0, sizeof(src_ip));

	dst_ip = (char *)malloc(16 * sizeof(char));
	memset(dst_ip, 0, sizeof(dst_ip));

	strcpy(iface, "eth0");

	if ((filedes = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("socket creation failed");
		return EXIT_FAILURE;
	}

	memset(&ifr, 0, sizeof(ifr));
	memcpy(&ifr.ifr_name, iface, sizeof(iface));
	if (ioctl(filedes, SIOCGIFHWADDR, &ifr) < 0){
		perror("ioctl failed to get mac");
		return EXIT_FAILURE;
	}
	
	close(filedes);
	
	memcpy(src_mac, ifr.ifr_hwaddr.sa_data, 6);
	
	printf("MAC ADDR FOR IFACE %s\t", iface);
	for (i=0; i < 6; i++){
		printf("%02x: ", src_mac[i]);
	}

	if((device.sll_ifindex = if_nametoindex(iface)) == 0){
		perror("if_nametoindex failed");
		return EXIT_FAILURE;
	}

	printf("\t index for iface %s is %d\n", iface, device.sll_ifindex);

	dst_mac[0] = 0x00;
        dst_mac[1] = 0x50;
        dst_mac[2] = 0x8b;
        dst_mac[3] = 0xe7;
        dst_mac[4] = 0xa6;
        dst_mac[5] = 0xf4;

	strcpy(src_ip, "193.178.153.136");
        strcpy(dst_ip, "193.178.153.253");

	device.sll_family = AF_PACKET;
	memcpy(device.sll_addr, src_mac, 6);
	device.sll_halen = htons(6);

	iph.ip_hl = IP4_HLEN / (sizeof(unsigned long int));
	iph.ip_v = 4;
	iph.ip_tos = 0;
	iph.ip_len = htons(IP4_HLEN + TCP_HLEN);
	iph.ip_id  = htons(0);
	iph.ip_off = 0;
	iph.ip_ttl = 255;
	iph.ip_p = IPPROTO_TCP;
	inet_pton(AF_INET, src_ip, &(iph.ip_src));
	inet_pton(AF_INET, dst_ip, &(iph.ip_dst));
	iph.ip_sum = 0;
	iph.ip_sum = checksum((unsigned short int*)&iph, IP4_HLEN);

	tcph.source = htons(8887);
	tcph.dest = htons(8887);
	//tcph.syn = htons(1);
	//tcph.ack = htons(1);
	//tcph.fin = htons(1);
	tcph.check = tcp_checksum(iph, tcph);

	frame_len  = 2 + 6 + 6 + IP4_HLEN + TCP_HLEN;

	memcpy(ether_frame, dst_mac, 6);
	memcpy(ether_frame + 6, src_mac, 6);
	ether_frame[12] = (ETH_P_IP / 256);
	ether_frame[13] = (ETH_P_IP % 256);

	memcpy(ether_frame + 14, &iph, IP4_HLEN);
	memcpy(ether_frame + 14 + IP4_HLEN, &tcph, TCP_HLEN);

	if ((filedes = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("socket creation failed");
		return EXIT_FAILURE;
	}
	
	if ((bytes = sendto(filedes, ether_frame, frame_len, 0,
	    (struct sockaddr*)&device, sizeof(device))) <= 0){
		perror("sendto failed");
		return EXIT_FAILURE;
	}
	
	close(filedes);

	free(src_mac);
	free(dst_mac);
	free(ether_frame);
	free(iface);
	free(src_ip);
	free(dst_ip);
	
	return EXIT_SUCCESS;
}

unsigned short int
checksum(unsigned short int *addr, int len)
{
	int nleft = len;
	int sum = 0;
	unsigned short int *w = addr;
	unsigned short int answer = 0;

	while (nleft > 1){
		sum += *w++;
		nleft -= sizeof(unsigned short int);
	}

	if (nleft == 1){
		*(unsigned char *)(&answer) = *(unsigned char *)w;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	answer = ~sum;
	return answer;
}

unsigned short int
tcp_checksum(struct ip iph, struct tcphdr tcph)
{
	char buff[IP_MAXPACKET];
	char *ptr;
	int chksumlen = 0;
	
	ptr = &buff[0];

	memcpy(ptr, &iph.ip_src.s_addr, sizeof(iph.ip_src.s_addr));
	ptr += sizeof(iph.ip_src.s_addr);
	chksumlen += sizeof(iph.ip_src.s_addr);

	memcpy(ptr, &iph.ip_dst.s_addr, sizeof(iph.ip_dst.s_addr));
	ptr += sizeof(iph.ip_dst.s_addr);
	chksumlen += sizeof(iph.ip_dst.s_addr);
	
	*ptr = 0;
	ptr++;
	chksumlen += 1;

	memcpy(ptr, &iph.ip_p, sizeof(iph.ip_p));
	ptr += sizeof(iph.ip_p);
	chksumlen += sizeof(iph.ip_p);

	memcpy(ptr, &tcph.source, sizeof(tcph.source));
	ptr += sizeof(tcph.source);
	chksumlen += sizeof(tcph.source);

	memcpy(ptr, &tcph.dest, sizeof(tcph.dest));
	ptr += sizeof(tcph.dest);
	chksumlen += sizeof(tcph.dest);

	return checksum((unsigned short int *)buff, chksumlen);
}
