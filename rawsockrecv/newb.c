#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <netinet/igmp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <net/if.h>


void packhandle(unsigned char *, int *, int *, int *, int *, int *, 
    int *, int *);
void print_ip(unsigned char *, int *);
void print_tcp(unsigned char *, int *);
void print_udp(unsigned char *, int *);
void print_icmp(unsigned char *, int *);
void print_igmp(unsigned char *, int *);

void 
packhandle(unsigned char *buff, int *size, int *tcp, int *udp, 
    int *icmp, int *igmp, int *others, int *total)
{
	struct __attribute__ ((packed)) iphdr *iph = (struct iphdr *)buff;
	
	(*total)++;

	if (iph->protocol == IPPROTO_ICMP){
		print_icmp(buff, size);
		(*icmp)++;
	}
	else if (iph->protocol == IPPROTO_IGMP){
		print_igmp(buff, size);
		(*igmp)++;
	}
	else if (iph->protocol == IPPROTO_TCP){
		print_tcp(buff, size);
		(*tcp)++;
	}
	else if (iph->protocol == IPPROTO_UDP){
		print_udp(buff, size);
		(*udp)++;
	}
	else 
		(*others)++; 

	printf("ICMP: %d IGMP: %d TCP: %d UDP: %d\n", *icmp, 
	    *igmp, *tcp, *udp);
	printf("Others: %d\t Total: %d\n", *others, *total);
}

void
print_ip(unsigned char *buff, int *size)
{
	struct __attribute__ ((packed)) iphdr *iph = (struct iphdr *)buff;	
	struct sockaddr_in src, dst;
	
	memset(&src, 0, sizeof(src));
	src.sin_addr.s_addr = iph->saddr;
	memset(&dst, 0, sizeof(dst));
	dst.sin_addr.s_addr = iph->daddr;
	
	printf("\n");
	printf("IP Version: %d\n", (unsigned int)iph->version);
	printf("IP Header Length: %d\n", (unsigned int)(iph->ihl)*4);
	printf("IP Type Of Service: %d\n", (unsigned int)iph->tos);
	printf("IP Total Length: %d\n", ntohs(iph->tot_len));
	printf("IP ID: %d\n", ntohs(iph->id));
	printf("IP Fragmentation Offset: %d\n", ntohs(iph->frag_off));
	printf("IP TTL: %d\n", (unsigned int)(iph->ttl));
	printf("IP Protocol: %d\n", (unsigned int)(iph->protocol));
	printf("IP Checksum: %d\n", ntohs(iph->check));
	printf("IP Source: %s\n", inet_ntoa(src.sin_addr));
	printf("IP Destination: %s\n", inet_ntoa(dst.sin_addr));
}

void
print_tcp(unsigned char *buff, int *size)
{
	struct __attribute__ ((packed)) iphdr *iph = (struct iphdr *)buff;
	unsigned short iphdrlen;
	iphdrlen = iph->ihl*4;
	struct __attribute__ ((packed)) 
	    tcphdr *tcph = (struct tcphdr*)(buff + iphdrlen);
	
	print_ip(buff, size);
	
	printf("\n");
	printf("TCP Header\n");	
	printf("TCP Source: %d\n", ntohs(tcph->source));
	printf("TCP Destination: %d\n", ntohs(tcph->dest));
	printf("TCP SEQ: %d\n", (unsigned int)(tcph->seq));
	printf("TCP ACK_SEQ: %d\n", (unsigned int)(tcph->ack_seq));
	
	printf("TCP Window: %d\n", ntohs(tcph->window));
	printf("TCP Checksum: %d\n", ntohs(tcph->check));
	printf("TCP Urgent Pointe: %d\n", ntohs(tcph->urg_ptr));
}
	 
void
print_udp(unsigned char *buff, int *size)
{
	struct __attribute__ ((packed))  iphdr *iph = (struct iphdr *)buff;
	unsigned short iphdrlen;
	iphdrlen = iph->ihl*4;
	struct __attribute__ ((packed)) 
	   udphdr *udph = (struct udphdr *)(buff + iphdrlen);

	print_ip(buff, size);	

	printf("\n");
	printf("UDP Header\n");
	printf("UDP Source: %d\n", ntohs(udph->source));
	printf("UDP Destination: %d\n", ntohs(udph->dest));
	printf("UDP Len: %d\n", ntohs(udph->len));
	printf("UDP Checksum: %d\n", ntohs(udph->check));
}

void
print_icmp(unsigned char *buff, int *size)
{
	struct __attribute__ ((packed)) iphdr *iph = (struct iphdr *)buff;	
	unsigned short iphdrlen;
	iphdrlen = iph->ihl*4;
	struct __attribute__ ((packed))
	    icmphdr* icmph = (struct icmphdr*)(buff + iphdrlen);
	print_ip(buff, size);
	
	printf("\n");
	printf("ICMP Header\n");
	printf("ICMP Type: %d\n", (unsigned int)(icmph->type));
	printf("ICMP Code: %d\n", (unsigned int)(icmph->code));
	printf("ICMP Checksum: %d\n", ntohs(icmph->checksum));
}

void
print_igmp(unsigned char *buff, int *size)
{
	struct __attribute__ ((packed))  iphdr *iph = (struct iphdr *)buff;
	unsigned short iphdrlen;
	iphdrlen = iph->ihl*4;
	
	struct __attribute__ ((packed))
	    igmp *igmph = (struct igmp*)(buff + iphdrlen);
	
	print_ip(buff, size);

	printf("\n");
	printf("IGMP Type: %d\n", (unsigned int)igmph->igmp_type);
	printf("IGMP Code: %d\n", (unsigned int)igmph->igmp_code);
	printf("IGMP Checksum: %d\n", ntohs(igmph->igmp_cksum));
}


static void 
printBuff(unsigned char *buff)
{	
	int i;	
	
	for (i = 0; i < 40; i++) {
		printf("%02x ", buff[i]);
	}
	printf("\n");
}	
	
int
main(int argc, char *argv[])
{
	struct sockaddr saddr;
	unsigned char *buff;
	int data_size;
	socklen_t saddr_size;	
	int sockfd;
	int tcp = 0;
	int udp = 0;
	int igmp = 0;
	int icmp = 0;
	int others = 0;
	int total = 0;

	buff = (unsigned char *)malloc(65536);
	
	if((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0){
		perror("Socket creation failed\n");
		exit(-1);
	}

	for(;;){
		unsigned char *buff_p;
		
		saddr_size = sizeof(saddr);
		data_size = recvfrom(sockfd, buff, 65536, 0,
		    &saddr, &saddr_size);
		if (data_size < 0){
			perror("recvfrom failed");
			exit(-1);
		}

		printBuff(buff);
		buff_p = buff;
		buff_p += ETH_HLEN;

 
 		packhandle(buff_p, &data_size, &tcp, &udp, &icmp, &igmp,
	    	    &others, &total);
	//	printBuff(buff);
	}
	close(sockfd);
	free(buff);
	return 0;
}
