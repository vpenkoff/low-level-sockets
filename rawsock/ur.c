#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

unsigned short
csum(unsigned short *ptr,int nbytes)
{
        register long sum;
        unsigned short oddbyte;
        register short answer;

        sum=0;
        while(nbytes>1) {
                sum+=*ptr++;
                nbytes-=2;
        }
        if(nbytes==1) {
                oddbyte=0;
                *((u_char*)&oddbyte)=*(u_char*)ptr;
                sum+=oddbyte;
        }

        sum = (sum>>16)+(sum & 0xffff);
        sum = sum + (sum>>16);
        answer=(short)~sum;

        return(answer);
}

struct
iphead
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
	unsigned int ihl:4;
	unsigned int version:4;
#elif __BYTE_ORDER == __BIG_ENDIAN
	unsigned int version:4;
	unsigned int ihl:4;
#else
# error "Please fix <bits/endian.h>"
#endif
	u_int8_t tos;
	u_int16_t tot_len;
	u_int16_t id;
	u_int16_t frag_off;
	u_int8_t ttl;
    	u_int8_t protocol;
    	u_int16_t check;
    	u_int32_t saddr;
    	u_int32_t daddr;
}__attribute__((packed));

struct
udphead
{
  	u_int16_t source;
  	u_int16_t dest;
  	u_int16_t len;
  	u_int16_t check;
}__attribute__((packed));

int
main(int argc, char* argv[])
{
        int on = 1;
        int sockfd;
        char *buff, source_ip[32], dest_ip[32], *data;
        struct sockaddr_in sin, din;
        buff = malloc(4096);
        memset(buff, 0, 4096);
        struct iphead *iph = (struct iphead *)buff;
        struct udphead *udph = (struct udphead *)(buff +
            sizeof(struct iphead));

        if (argc < 5){
                perror("Usage: <source ip> <source port>" " "
                    "<dest ip> <dest port>");
                exit(EXIT_SUCCESS);
        }

        if((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) == -1){
                perror("Socket creation failed");
                exit(EXIT_SUCCESS);
        }

        setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL,
            (const char *)&on, sizeof(on));
        data = buff + sizeof(struct iphead) + sizeof(struct udphead);
        strcpy(data, "UNIX");

        strcpy(source_ip, argv[1]);
        strcpy(dest_ip, argv[3]);

        sin.sin_family = AF_INET;
        sin.sin_port = htons(atoi(argv[2]));
        sin.sin_addr.s_addr = inet_addr(argv[1]);

        din.sin_family = AF_INET;
        din.sin_port = htons(atoi(argv[4]));
        din.sin_addr.s_addr = inet_addr(argv[3]);

        iph->ihl = 5;
        iph->version = 4;
        iph->tos = 0;
        iph->tot_len = sizeof(struct iphead) + sizeof(struct udphead)
            + strlen(data);
        iph->id = htonl(666);
        iph->frag_off = 0;
        iph->ttl = 16;
        iph->protocol = IPPROTO_UDP;
        iph->saddr = inet_addr(source_ip);
        iph->daddr = inet_addr(dest_ip);
        iph->check = csum((unsigned short *)buff, iph->tot_len);

        udph->source = htons(atoi(argv[2]));
        udph->dest = htons(atoi(argv[4]));
        udph->len = htons(sizeof(struct udphead) + strlen(data));
        udph->check = csum((unsigned short* )buff, udph->len);


        int i;
        for(i=0; i<50; i++){
                if (sendto(sockfd, buff, iph->tot_len, 0,
                    (struct sockaddr *)&din, sizeof(din)) < 0){
                perror("sendto failed");
                }
                else{
                        printf("Packet send, Length: %d\n",
                            iph->tot_len);
                }
                sleep(1);
        }
                close(sockfd);
                return 0;
}
	
