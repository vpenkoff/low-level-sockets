#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define PORT 1234
#define BUFSIZE 128

  void
echo(int* sockfd, struct sockaddr* cli,
    socklen_t clilen)
{
  int n;
  socklen_t len;
  char msg[BUFSIZE];

  for (;;) {
    len = clilen;
    n = recvfrom(*sockfd, msg, BUFSIZE,
        0, cli, &len);

    sendto(*sockfd, msg, n, 0, cli,
        len);
    printf("%s\n", msg);
    memset(&msg, 0, sizeof(msg));
  }
}


  int
main(int argc, char *argv[])
{
  struct sockaddr_in server, client;
  int sockfd;
  int optaddr;

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sockfd < 0)
    perror("Socket creation failed");

  memset(&server, 0, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = 0;/* htonl(INADDR_ANY); */
  server.sin_port = htons(PORT);

  printf("Server-socket() is OK...\n");

  if((setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optaddr,
          sizeof(optaddr)) < 0))
    perror("Setsockopt failed: REUSEADDR");

  if(bind(sockfd, (struct sockaddr*)&server,
        sizeof(server)) < 0)
    perror("Bind failed");

  printf("Server-bind() is OK...\n");

  printf("Server is ready...\n");
  echo(&sockfd, (struct sockaddr*)&client,
      sizeof(client));

  return EXIT_SUCCESS;
}
