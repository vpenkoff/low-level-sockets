#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define PORT 1234
#define BUFSIZE 128

  void
echo(int *sockfd, const struct sockaddr* serveraddr, socklen_t len)
{
  int n;
  char sendmsg[BUFSIZE], recvmsg[BUFSIZE];

  while(fgets(sendmsg, BUFSIZE, stdin) != NULL){
    sendto(*sockfd, sendmsg, strlen(sendmsg), 0, serveraddr, len);

    n = recvfrom(*sockfd, recvmsg, BUFSIZE, 0, NULL, NULL);
    recvmsg[n] = 0;
    fputs(recvmsg, stdout);
  }
}

  int
main(int argc, char* argv[])
{
  int sockfd;
  struct sockaddr_in server;
  struct hostent *host_info;

  if (argc != 2)
    perror("Usage: client serveraddr");

  memset(&server, 0, sizeof(server));

  if ((inet_pton(AF_INET, argv[1], &server)) < 1){
    host_info = gethostbyname(argv[1]);
    if (NULL==host_info)
      perror("Server unknown");
    memcpy((char*)&server.sin_addr, host_info->h_addr,
        host_info->h_length);
  }

  server.sin_family = AF_INET;
  server.sin_port = htons(PORT);

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);

  echo(&sockfd, (struct sockaddr*)&server, sizeof(server));

  return EXIT_SUCCESS;
}
